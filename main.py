import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import DateEntry


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.title("Formulário de Registro")
        self.state('zoomed')  # Maximiza a janela para ocupar a tela inteira
        self.iconbitmap('icone.ico')  # Define o ícone da janela (substitua 'icone.ico' pelo nome do seu arquivo de ícone)

        # Estilo
        style = ttk.Style(self)
        style.theme_use("clam")
        style.configure("TLabel", font=("Helvetica", 14))
        style.configure("TEntry", font=("Helvetica", 14))
        style.configure("TButton", font=("Helvetica", 14, "bold"), background="#4CAF50", foreground="#ffffff")
        style.configure("TFrame", background="#f0f0f0")

        # Frame principal
        frame = ttk.Frame(self, padding="20")
        frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        # Frame para os inputs
        input_frame = ttk.Frame(frame, padding="10", relief="solid", borderwidth=1)
        input_frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S), padx=10, pady=10)
        
        # Configuração do grid para input_frame
        for i in range(10):
            input_frame.grid_rowconfigure(i, weight=1, pad=10)
        for i in range(2):
            input_frame.grid_columnconfigure(i, weight=1, pad=10)

        # Validação para entradas numéricas com formatação
        vcmd_numeric = (self.register(self.validate_and_format_numeric_input), '%P', '%W')
        vcmd_codigo_rt = (self.register(self.validate_and_format_codigo_rt), '%P', '%W')

        # Nome do Operador
        ttk.Label(input_frame, text="Nome do Operador:").grid(row=0, column=0, sticky=tk.W)
        self.operador_entry = ttk.Entry(input_frame, width=40)
        self.operador_entry.grid(row=0, column=1, sticky=tk.W)

        # Código RT
        ttk.Label(input_frame, text="Código RT:").grid(row=1, column=0, sticky=tk.W)
        self.codigo_rt_entry = ttk.Entry(input_frame, width=40, validate='key', validatecommand=vcmd_codigo_rt)
        self.codigo_rt_entry.grid(row=1, column=1, sticky=tk.W)

        # Data e Hora de Chegada
        ttk.Label(input_frame, text="Data e Hora de Chegada:").grid(row=2, column=0, sticky=tk.W)
        chegada_frame = ttk.Frame(input_frame)
        chegada_frame.grid(row=2, column=1, sticky=tk.W)
        self.data_chegada_entry = DateEntry(chegada_frame, width=20, background='darkblue', foreground='white', borderwidth=2, date_pattern='dd/MM/yyyy')
        self.data_chegada_entry.grid(row=0, column=0, sticky=tk.W)
        self.hora_chegada_entry = ttk.Combobox(chegada_frame, width=10, values=[f'{h:02}:00' for h in range(24)])
        self.hora_chegada_entry.grid(row=0, column=1, padx=5, sticky=tk.W)

        # Data e Hora de Início
        ttk.Label(input_frame, text="Data e Hora de Início:").grid(row=3, column=0, sticky=tk.W)
        inicio_frame = ttk.Frame(input_frame)
        inicio_frame.grid(row=3, column=1, sticky=tk.W)
        self.data_inicio_entry = DateEntry(inicio_frame, width=20, background='darkblue', foreground='white', borderwidth=2, date_pattern='dd/MM/yyyy')
        self.data_inicio_entry.grid(row=0, column=0, sticky=tk.W)
        self.hora_inicio_entry = ttk.Combobox(inicio_frame, width=10, values=[f'{h:02}:00' for h in range(24)])
        self.hora_inicio_entry.grid(row=0, column=1, padx=5, sticky=tk.W)

        # Data e Hora de Término
        ttk.Label(input_frame, text="Data e Hora de Término:").grid(row=4, column=0, sticky=tk.W)
        termino_frame = ttk.Frame(input_frame)
        termino_frame.grid(row=4, column=1, sticky=tk.W)
        self.data_termino_entry = DateEntry(termino_frame, width=20, background='darkblue', foreground='white', borderwidth=2, date_pattern='dd/MM/yyyy')
        self.data_termino_entry.grid(row=0, column=0, sticky=tk.W)
        self.hora_termino_entry = ttk.Combobox(termino_frame, width=10, values=[f'{h:02}:00' for h in range(24)])
        self.hora_termino_entry.grid(row=0, column=1, padx=5, sticky=tk.W)

        # Nome do Motorista
        ttk.Label(input_frame, text="Nome do Motorista:").grid(row=5, column=0, sticky=tk.W)
        self.motorista_entry = ttk.Entry(input_frame, width=40)
        self.motorista_entry.grid(row=5, column=1, sticky=tk.W)

        # Quantidade
        ttk.Label(input_frame, text="Quantidade:").grid(row=6, column=0, sticky=tk.W)
        self.quantidade_entry = ttk.Entry(input_frame, width=40, validate='key', validatecommand=vcmd_numeric)
        self.quantidade_entry.grid(row=6, column=1, sticky=tk.W)

        # Quantidade Faltante
        ttk.Label(input_frame, text="Quantidade Faltante:").grid(row=7, column=0, sticky=tk.W)
        self.quantidade_faltante_entry = ttk.Entry(input_frame, width=40, validate='key', validatecommand=vcmd_numeric)
        self.quantidade_faltante_entry.grid(row=7, column=1, sticky=tk.W)

        # Status
        ttk.Label(input_frame, text="Status:").grid(row=8, column=0, sticky=tk.W)
        self.status_combobox = ttk.Combobox(input_frame, width=40, values=["Em Processo", "Finalizado"], state='readonly')
        self.status_combobox.grid(row=8, column=1, sticky=tk.W)
        self.status_combobox.current(0)  # Define "Em Processo" como valor padrão

        # Botão para enviar


        # Botão para enviar
        self.submit_button = ttk.Button(frame, text="Enviar", command=self.submit)
        self.submit_button.grid(row=1, column=0, pady=20)

        # Label para mensagens de status
        self.status_label = ttk.Label(frame, text="", font=("Helvetica", 14), foreground="green")
        self.status_label.grid(row=2, column=0, pady=10)

    def validate_and_format_numeric_input(self, new_value, widget_name):
        """Função para validar e formatar se o novo valor é numérico"""
        if new_value == "":
            return True
        if new_value.replace('.', '').isdigit():
            formatted_value = self.format_number(new_value)
            self.set_entry_value(widget_name, formatted_value)
            return False  # Prevents the default behavior since we manually set the value
        else:
            return False

    def validate_and_format_codigo_rt(self, new_value, widget_name):
        """Função para validar e formatar o Código RT"""
        if new_value == "":
            return True
        new_value = new_value.replace('-', '')
        formatted_value = self.format_codigo_rt(new_value)
        self.set_entry_value(widget_name, formatted_value)
        return False

    def format_number(self, value):
        """Formata o número com ponto na casa dos mil"""
        parts = value.split('.')
        if len(parts) == 1:
            return '{:,}'.format(int(parts[0])).replace(',', '.')
        else:
            return '{:,}'.format(int(parts[0])).replace(',', '.') + '.' + parts[1]

    def format_codigo_rt(self, value):
        """Formata o Código RT com hífen a cada três caracteres"""
        return '-'.join([value[i:i+3] for i in range(0, len(value), 3)])

    def set_entry_value(self, widget_name, value):
        """Define o valor do campo de entrada"""
        widget = self.nametowidget(widget_name)
        widget.delete(0, tk.END)
        widget.insert(0, value)

    def submit(self):
        if not self.validate_all_fields():
            self.status_label.config(text="Por favor, preencha todos os campos.", foreground="red")
            messagebox.showerror("Erro", "Por favor, preencha todos os campos.")
            return

        dados = {
            "Nome do Operador": self.operador_entry.get(),
            "Código RT": self.codigo_rt_entry.get(),
            "Data e Hora de Chegada": f"{self.data_chegada_entry.get()} {self.hora_chegada_entry.get()}",
            "Data e Hora de Início": f"{self.data_inicio_entry.get()} {self.hora_inicio_entry.get()}",
            "Data e Hora de Término": f"{self.data_termino_entry.get()} {self.hora_termino_entry.get()}",
            "Nome do Motorista": self.motorista_entry.get(),
            "Quantidade": self.quantidade_entry.get(),
            "Quantidade Faltante": self.quantidade_faltante_entry.get(),
            "Status": self.status_combobox.get()
        }
        self.status_label.config(text="Dados submetidos com sucesso!", foreground="green")
        messagebox.showinfo("Dados Submetidos", f"Dados submetidos:\n{dados}")

    def validate_all_fields(self):
        """Valida se todos os campos foram preenchidos"""
        fields = [
            self.operador_entry.get(),
            self.codigo_rt_entry.get(),
            self.data_chegada_entry.get(),
            self.hora_chegada_entry.get(),
            self.data_inicio_entry.get(),
            self.hora_inicio_entry.get(),
            self.data_termino_entry.get(),
            self.hora_termino_entry.get(),
            self.motorista_entry.get(),
            self.quantidade_entry.get(),
            self.quantidade_faltante_entry.get(),
            self.status_combobox.get()
        ]
        return all(fields)

if __name__ == "__main__":
    app = App()
    app.mainloop()
